﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ArztVerwaltung.Startup))]
namespace ArztVerwaltung
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
