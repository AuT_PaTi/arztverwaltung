﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArztVerwaltung.Models
{
    public class VisitDrug
    {
        [Key]
        public int VisitDrugID { get; set; }
        [Required, Range(1,10)]
        public int Amount { get; set; }

        public int VisitID { get; set; }
        public int DrugID { get; set; }

        public virtual Visit Visit { get; set; }
        public virtual Drug Drug { get; set; }
    }
}