﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ArztVerwaltung.Models
{
    public class Visit
    {
        [Key]
        public int VisitID { get; set; }
        [Required]
        [Display(Name = "Datum")]
        public DateTime Date { get; set; }

        public string DoctorID { get; set; }
        [ForeignKey("DoctorID")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        public int ClientID { get; set; }
        public virtual Client Client { get; set; }

        public virtual ICollection<VisitDrug> VisitDrugs { get; set; }
    }
}