﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArztVerwaltung.Models
{
    public class Client
    {
        [Key]
        public int ClientID { get; set; }
        [Display(Name = "Vorname")]
        [StringLength(55)]
        [Required]
        public string FName { get; set; }
        [Display(Name = "Nachname")]
        [StringLength(55)]
        [Required]
        public string LName { get; set; }
        [Display(Name = "Geburtstag")]
        public DateTime Birthday { get; set; }
        [Display(Name = "Name")]
        public string FullName { get { return FName + " " + LName; } }

        public virtual ICollection<Visit> Visits { get; set; }
    }
}