﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ArztVerwaltung.Models
{
    public class Context : ApplicationDbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<Drug> Drugs { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<VisitDrug> VisitDrugs { get; set; }
    }
}