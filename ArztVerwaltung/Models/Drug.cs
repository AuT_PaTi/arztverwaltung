﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArztVerwaltung.Models
{
    public class Drug
    {
        [Key]
        public int DrugID { get; set; }
        [Required]
        [StringLength(55)]
        [Display(Name = "Name")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Preis")]
        public decimal Price { get; set; }

        public virtual ICollection<VisitDrug> VisitDrugs { get; set; }
    }
}