﻿using ArztVerwaltung.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArztVerwaltung.Controllers
{
    public class VisitsController : Controller
    {
        private Context db = new Context();
        // GET: Visits
        public ActionResult Index()
        {
            return View(db.Visits.ToList());
        }
        public ActionResult Create()
        {
            ViewBag.ApplicationUserList = new SelectList(db.Users.ToList(), "Id", "FullName");
            ViewBag.ClientList = new SelectList(db.Clients.ToList(), "ClientID", "FullName");
            return View();
        }
        [HttpPost]
        public ActionResult Create([Bind]Visit visit)
        {
            if (ModelState.IsValid)
            {
                db.Visits.Add(visit);
                return RedirectToAction("Index");
            }
            return View(visit);
        }
    }
}