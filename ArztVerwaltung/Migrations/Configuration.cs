namespace ArztVerwaltung.Migrations
{
    using ArztVerwaltung.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ArztVerwaltung.Models.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
        /// <summary>
        /// Wird aufgerufen wenn zu einer neuen Version migriert wird.
        /// </summary>
        /// <param name="context">Datenbank Context</param>
        protected override void Seed(ArztVerwaltung.Models.Context context)
        {
            if (!context.Users.Any(r => r.UserName == "StefanNeuner"))
            {
                ApplicationUserManager manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
                var user = new ApplicationUser { FName = "Stefan", LName = "Neuner", Email = "sneuner@gmail.com", UserName = "StefanNeuner" };
                manager.Create(user, "AsdfAsdf!");
            }
            Client client = new Client { ClientID = 3, FName = "Alex", LName = "Neurauter", Birthday = DateTime.Parse("6.7.2004") };
            context.Clients.AddOrUpdate(
                new Client { ClientID = 1, FName = "Patrick", LName = "Eccher", Birthday = DateTime.Parse("5.1.2002")},
                new Client { ClientID = 2, FName = "Max", LName = "Mustermann", Birthday = DateTime.Parse("3.2.2001")},
               client
                );
            context.Drugs.AddOrUpdate(
                new Drug { DrugID = 1, Title = "Paracetamol", Price = 12.99m},
                new Drug { DrugID = 2, Title = "Pantoprazol", Price = 29.99m}
                );
        }
    }
}
